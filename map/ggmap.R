#!/usr/bin/Rscript

library(ggmap)

uv <- read.csv("UV_20151116152215.csv")

png(file="~/Downloads/UV.png")
Sys.setenv(http_proxy="http://192.168.60.95:8118")
map <- get_map(location = 'Taiwan', zoom = 7)
ggmap(map) + geom_point(aes(x = WGS84Lon, y = WGS84Lat, size = UVI), data = uv)
dev.off()

